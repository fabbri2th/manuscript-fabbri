\chapter{FSI solver}
\label{ch4}
\graphicspath{ {./img/ch4/}}

\textit{This chapter describes the FSI solver, which couples the ALE solver and the SMS previously presented. It begins with the presentation of the original
mesh movement method developed to deform the fluid mesh according to the solid displacement. The FSI coupling schemes are then 
introduced progressively and applied to a simple test case, and their stability in function of a given parameter is studied. 
It finishes with the scheme used in the rest of this work.}

\adjustmtc[0]

\minitoc


\section{Mesh Movement Solver}
\label{ch4_sec_mms}
\subsection{Pseudo solid method}
\label{ch4_sec_PS}
% thèse DeNayer 
% 115
% ça semblait naturel de faire ça étant donné que le SMS venait d'être dev et que ça a rapidement donné des bons résultats
As discussed in section~\ref{ch1_sec_mono_part}, immersed boundaries methods do not allow the precise description of geometries 
which is critical to predict accurately boundary layers in turbulent cases. Therefore, body fitted meshes are prefered. Besides, the 
FSI coupling takes place at the interface between fluid and structure, so that the fluid domain has to move according to the solid displacement. 
This imposes to compute a grid movement for the entire fluid domain while maintaining suitable cells quality. This is particularly 
challenging for cases with large deflection on unstructured grid, because the mesh has no preferential direction contrary to structured mesh. 
This facilitates the distribution of the deformation among cells with algebraic methods such as transfinite interpolation 
(TFI)~\cite{thompson1985numerical,spekreijse1995elliptic,spekreijse2002simple,potsdam2001parallel}, tension spring analogy~\cite{batina1990unsteady} or torsion 
spring analogy~\cite{farhat1998torsional}. These methods are either not appropriate for unstructured meshes because it implies interpolation along mesh lines, or 
too expensive for large scale problems, as explained in~\cite{sheng2013efficient}.


Furthermore, to be able to reproduce numerically fluid structure interaction cases with turbulent flows, 
mesh movement method has to be suitable for a grid with a large number of elements and different mesh size variation, 
especially for LES which requires fine grid resolution. However, precision of spatial schemes suffer from mesh distorsion~\cite{bernard2020high_order_framework}, 
so that it is preferable to avoid to deform the mesh in regions where strong velocity gradients occur. Thus, mesh movement requires an algorithm that can be 
fully parallelized and allowing to control which regions of the domain will withstand the deformation. These reasons explain the choice the pseudo-solid 
method~\cite{de2008interaction,lefranccois2008simple} for the present work. Besides, considering that the SMS had been developed just before, the choice of
and algorithm based on the FEM seemed very natural. Note that alternatives will be discussed as perspective of this work in section~\ref{ch7_sec_perspective}.

The principle of this method is to consider the fluid domain as a linear elastic solid. 
The displacement constrains that the domain undergo then become usual boundaries conditions of a solid with prescribed displacement, as seen in section~\ref{ch3_sec_dis_bnd}. 
Inertial effects are not advisable here so the problem is considered stationary. 
The mesh movement consists then in solving a linear static problem. With a FEM formulation, it gives 
%
\begin{equation}
\label{ch4_eq_ps}
\mathbf{K}\mathbf{d} = \mathbf{f}
\end{equation}
%
with $\mathbf{K}$ the stiffness matrix, $\mathbf{d}$ the nodes displacement and $\mathbf{f}$ the forces. 
With this formulation, the equilibrium position of the pseudo-solid is considered to be the mesh configuration at $t=0=t_0$. 
Here, there is no external forces such as body forces or pressure on boundaries, however $\mathbf{f}$ depends on the displacement conditions.
In this work, this method has been improved to meet FSI simulations requirements. This is detailed in the next two sections. 
%
\subsection{Implementation in YALES2}
\label{ch4_sec_ps}
% Idée de se servir de la distance algo Romain lvl set
% voir papier (E=f(R), noeuds slide sur les walls)
% présentation du tests
As explained above, it is very useful to select regions of the domain that will move without cell deformation, and the zone 
that will withstand the deformation. The pseudo-solid method is very convenient for this purpose because it only requires to change the 
element pseudo-Young modulus $E$, that will directly affect the cell flexibility. In fact, for an heterogeneous solid, the part that will 
deform first is the less rigid one. Moreover, in most cases, the smallest cells are regrouped close to the flexible body because 
it corresponds to the zone of interest, and it is essential to maintain the quality of these cells. Therefore, the generic technique developed 
here to compute the pseudo-Young modulus field of the pseudo-solid consists in establishing a $E$ profile as a function of the distance from the object $R$.   

In this work, this distance $R$ is computed with a geometrical method for unstructured simplicial meshes~\cite{janodet2019unstructured}, already used 
in two phases flow solvers of YALES2 to compute the distance to the phases interface~\cite{pertant2021finite}. 
The pseudo-Poisson ratio of all elements is $0.2$ and their pseudo-Young modulus $E$ is computed in function of $R$ such as 
%
\begin{subequations}
\label{ch4_eq_ps_all}
\begin{empheq}[left = \empheqlbrace]{align}
&\  E(R<R_{min}) = 100E_{int}  \\
&\  E(R_{min}\leq R \leq R_{max}) = E_{int} - \frac{(R-R_{min})(E_{int}-E_{min})}{R_{max}-R_{min}} \\
&\  E(R>R_{max}) = E_{min} \\
&\  E_{int} = E_{min}  y_r 
\end{empheq}
\end{subequations}
%
where the pseudo-Young modulus ratio $y_r$, $R_{min}$ and $R_{max}$ are user's parameters. 
On the other hand, the value of $E_{min}$ has no influence since it is a linear elastic problem. 
This method ensures that for $R<R_{min}$, deformation is minimal and cells move mainly according to the imposed displacement, 
thanks to the high jump of $E$ at $R_{min}$. On the contrary, the mesh deformation takes place in the transition zone 
between $R_{min}$ and $R_{max}$. The size of the different regions can be easily adapted for the different cases with these two parameters. 
However the deformation is not uniform in the transition zone, because for $y_r=1$, only cells at $R=R{min}$ are deformed, and for 
high value of $y_r$, most of the deformation will be taken by cells at $R\approx R_{max}$. The suitable value for $y_r$ then depends on the 
size of the transition zone, the displacement amplitude and the cells size distribution. An example of both $R$ and $E$ fields can 
be visualized in Fig.~\ref{ch4_fig_distance_E}, where the geometry used for the example corresponds to the case presented in Chapter~\ref{ch5}.
\begin{figure}[!h]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\linewidth]{oct_R.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\linewidth]{oct_E.png}
    \end{subfigure}
    \caption{Distance field from the object $R$ (left) and pseudo-Young modulus field $E$ (right).}
    \label{ch4_fig_distance_E}
\end{figure}
%Sliding des noeuds 

% Limite
This method has been proven to be efficient but could be improved; the cells quality of the elements with the lowest 
$E$ frequently dropped first, but the deformation did not sufficiently affect cells closer to the object so that 
the computations stopped and failed even though cells at $R_{min}\leq R \leq R_{max}$ were still nearly non deformed. 

\subsection{Adjustment of pseudo-material properties}
\label{ch4_sec_adj}
% voir papier, comp avec / sans
% limite
In order to improve the previous method, the idea is to make the cells pseudo-Young modulus $E$ depend on their quality. 
The latter can be determined quantitatively by computing the cells skewness $S$ defined by Eq.(\ref{ch2_eq_skew}).
This criterion is then used to adjust the pseudo-solid elements flexibility during the simulation such as 
\begin{equation}
\label{ch4_eq_adj}
E(t_{n+1},S(t_{n})) = \frac{\big[ E_{int}-E(t_0) \big] \big[ S(t_n)-S(t_0) \big] }{S_{max}-S(t_0)}+E(t_0).
\end{equation}
where $S_{max}$ is chosen at $0.90$. Note that the pseudo-Young modulus at the beginning of the computation $E(t_0)$ is 
established according to the Eq.(\ref{ch4_eq_ps_all}). Equation(\ref{ch4_eq_adj}) is applied only for cells where $R>R_{min}$ because the 
properties of cells close to the object has to be homogeneous as most as possible to guarantee that these elements remain undeformed. 

However, this single modification is not enough to considerably improve the method. In fact, the solving of Eq.(\ref{ch4_eq_ps}) for a strong displacement 
will result in a mesh with a deformed cells zone. That will induce an increase of the pseudo-Young modulus of these cells because of Eq.(\ref{ch4_eq_adj}), 
so that they will become more rigid than their neighbours. The solving at the next time step will then lead to a mesh where that same zone has 
not been deformed because of the previously computed high value of $E$, but their neighbours on the contrary will withstand all the deformation. 
The issue here is that the solved problem is still defined according to the initial configuration, and therefore the cell skewness $S$ cannot evolve progressively. 
To make this technique efficient, the reference configuration used to compute the stiffness matrix $\mathbf{K}$ 
has to be updated at each time step with the new node position, which means to update continuously the equilibrium position of the pseudo-solid. 
The computed displacement becomes then an increment $d\mathbf{d}_{\tn{n+1}}$ used to move nodes between their position at $t_n$ and the new one at $t_{n+1}$. Besides, to prevent 
high gradient of $E$, the pseudo-Young modulus computed with Eq.(\ref{ch4_eq_adj}) is filtered once with a scatter-gather algorithm, given that this data is stored 
at the element. Tests showed that this strategy improves significantly the efficiency of the algorithm. 

An example of application of the full method is given in Fig.~\ref{ch4_fig_mm_snap}. 
It shows skewness fields of a test case where a domain boundary, the rod behind the cylinder, moves according a prescribed sinusoidal displacement at different instants. 
\begin{figure*}[!h]
    \centering
    \includegraphics[width=1.0\linewidth]{ensemble_snap.png}
    \caption{Skewness field of a test case with imposed displacement for the moving boundary at different instants.}
    \label{ch4_fig_mm_snap}
\end{figure*}
For this test, a mesh with inhomogeneous cell size has been used; close to the cylinder and the rod, a zone with small elements can be identified.
The mesh movement strategy has to ensure that these cells will not be too much deformed as explained in previous section. With the present method, 
the good behaviour is obtained by choosing a correct value for $R_{min}$, and the different snapshots highlight that the skewness in this region remains intact. 
Moreover, it can also be seen that the two critical zones in terms of skewness progressively appear; one at $R=R_{min}$ and the other at $R=R_{max}$. 
That can be explained by the stronger pseudo-Young modulus gradients of these regions. Nevertheless, the two last snapshots prove that 
the rigidification of these cells leads to a smooth cell deformation, resulting in a satisfying skewness distribution. 

A comparison of the method of section~\ref{ch4_sec_ps} and the method of section~\ref{ch4_sec_adj} is given in Fig.~\ref{ch4_fig_comp_adj}. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{comp_adj_ppt_23_07.PNG}
\caption{Comparison of the pseudo-solid method of section~\ref{ch4_sec_ps} (top) and the method of section~\ref{ch4_sec_adj} (bottom).}
\label{ch4_fig_comp_adj}
\end{center}
\end{figure*}
It shows that without the new formulation, only elements with low value of $E$ tend to deform, while the other cells in the transition zone remain nearly unchanged. 
With the adjustment of pseudo-material elements properties, it is clear that the skewness field is much more homogeneous, thanks to the rigidification of close wall cells. 
This last technique appeared consequently more robust to handle strong displacement and maintain a lower maximum skewness. 
An example of pseudo-Young modulus field after a large displacement is given in Fig.~\ref{ch4_fig_champ_E_post_adj}. 
It emphasizes that regions forced to stretch see their cell skewness decrease, and on the other hand compressed regions undergo growing skewness. 
This causes a pseudo-Young modulus profile quite different from the one at $t=0$ given Fig.~\ref{ch4_fig_distance_E}.
\begin{figure}[!h]%[]
\centering
\includegraphics[width=0.75\linewidth]{oct_E2.png}
\caption{Pseudo-Young modulus field after large deflection of the rod while adjusting the pseudo-material properties with Eq.(\ref{ch4_eq_adj}).}
\label{ch4_fig_champ_E_post_adj}
\end{figure}
This efficient mesh deformation method presents then the advantage to be robust, easy to implement and without parallelization issues. 
Furthermore, it allows the user to control which mesh region can be deformed or not.

Nevertheless, for certain cases with very large deformation, moving or rotating object, DMA is used (see section~\ref{ch2_sec_DMA}).
To combine efficiently this feature with the pseudo-material method, the stiffness matrix $\mathbf{K}$ has to be updated at each re-meshing step. 
In fact, a new mesh with a low maximum skewness has to be considered as a new equilibrium position. 
Besides, the previous $E$ field resulting from adjusting the pseudo-Young modulus as a function of skewness becomes irrelevant because the skewness 
field is completely changed. The distance from the object $R$ is then recomputed and a new pseudo-Young modulus field is established with Eq.(\ref{ch4_eq_ps_all}).
The entire method for mesh movement developed in this work is summarized in Fig.~\ref{ch4_fig_mms_scheme}.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=0.8\linewidth]{MMS_scheme_2.png}
\caption{Scheme of the mesh movement method developed in this work.}
\label{ch4_fig_mms_scheme}
\end{center}
\end{figure*}

Finally, it must also be noticed that the chosen time step can have a strong influence on the reversebility of the algorithm. 
Figure~\ref{ch4_fig_courbes} shows the evolution of the maximum skewness of the domain in function of time for the previously introduced case with 
a $\tau$ periodic prescribed motion of the rod. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{skew_comp_dt.pdf}
\caption{Comparison of the maximum skewness obtained for different timestep $\Delta t$. DMA is not used here.}
\label{ch4_fig_courbes}
\end{center}
\end{figure*}
It can be seen that for $\Delta t/\tau = 2.5 \times 10^{-2}$ the maximum skewness is increasing for each cycle of prescribed displacement, even if the 
geometry returns to the initial configuration every $\tau/2$ seconds. This is caused by the incremental formulation used with the adjustment of properties.
On the other hand, with a time step ten times smaller, this effect vanishes nearly entirely. Note that DMA is not used here to highlight this effect.
In practice, considering the time step necessary for FSI computations, 
this effect remains acceptable and is not restrictive because if the maximum skewness finally reach too high value ($\|S\|_{\infty}<S_{lim}$ with $S_{lim}=0.99$), 
the DMA can still be used.

%pb skewness finit par augmenter
\newpage

\section{FSI coupling}
\label{ch4_sec_FSI_coupling}
\subsection{Data transfer via CWIPI}
\label{ch4_sec_CWIPI}
% \subsection{Previous use of CWIPI}
% % dev onera ou Cerfacs?
% % thèse lancelot et sinon?
% \subsection{Adaptation to FSI solving}
% % parler dernier papier DeNayer problème que ça peut causer (voir mail DN)
% % adaptation ordre 2 car CWIPI gère pas tte les faces
FSI simulations consist in coupling fluid (ALE) and solid (SMS) solvers. The coupling conditions occur at the interface $\Gamma$ between fluid and solid as,
\begin{subequations}
% \label{eq_fsi_all}
\begin{empheq}[left = \empheqlbrace]{align}
% \label{eq_fsi_1}
&\  \mathbf{u}_{n+1} = \mathbf{w}_{n+1} = \dfrac{\mathbf{d}_{n+1}-\mathbf{d}_{n}}{\Delta t}  \\
% \label{eq_fsi_2}
&\  \mathbf{f}_{n+1} = \int_{\Gamma}\left( \mu \dfrac{\partial \mathbf{u}_{n+1}}{\partial n}+ P_{n+1}\mathbf{n} \right) d\Gamma
\end{empheq}
\end{subequations}
with $\mu$ the dynamic viscosity  and $\mathbf{n}$ the normal direction to $\Gamma$ pointing on the solid to fluid direction. 
In fact, solid imposes its velocity to fluid while fluid applies a force on solid through viscous shear and pressure. As a partitioned 
approach is chosen here (see section~\ref{ch1_sec_num_FSI}), the interface $\Gamma$ displacement $\mathbf{d}$ has to be sent 
from the SMS to the ALE solver, and the fluid forces 
applied on $\Gamma$ $\mathbf{f}$ has to be sent from the ALE solver to the SMS. 

The coupling is then performed by data exchange between solvers at the interface $\Gamma$ ensured by the CWIPI (Coupling With Interpolation Parallel Interface)
library~\cite{duchaine2011first,quemerais2016coupling}. 
This library allows to compute data interpolation based on boundary nodes coordinates, which is mandatory when meshes are not coincident. 
Nonetheless, the library has not been designed to support quadratic elements. As a FSI simulation involves surfacic coupling, faces of the 
quadratic elements used by the SMS have to be cut into linear elements, as shown in Fig.~\ref{ch4_fig_CWIPI_order2}.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{CWIPI_order2.png}
\caption{Adaptation of quadratic elements faces to make the SMS work with CWIPI.}
\label{ch4_fig_CWIPI_order2}
\end{center}
\end{figure*}

CWIPI can work with two meshes composed of different element type, with different cell sizes, as illustrated in Fig~\ref{ch4_fig_CWIPI_DN2}. 
It also manages the parallel communications 
allowing to benefit of the massively parallel performance of each solver. 
% \begin{figure*}[htbp]
% \begin{center}
% \includegraphics[width=1.0\linewidth]{CWIPI_DN2.png}
% \caption{Data transfer via CWIPI from fluid mesh composed with tetrahedrons of size $\Delta \tn{x}_f$ (left) to solid mesh 
% composed with 27-nodes hexahedrons of size $\Delta \tn{x}_s = 11 \Delta \tn{x}_f$ (right).}
% \label{ch4_fig_CWIPI_DN2}
% \end{center}
% \end{figure*}
\begin{figure}[htbp]
    \centering
    \begin{subfigure}[b]{1.0\textwidth}
        \includegraphics[width=1.0\linewidth]{CWIPI_DN2.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\textwidth}
        \includegraphics[width=1.0\linewidth]{CWIPI_zoom_DN2_c.png}
    \end{subfigure}
    \caption{Data transfer via CWIPI from fluid mesh composed with tetrahedrons of size $\Delta \tn{x}_f$ (left) to solid mesh 
    composed with 27-nodes hexahedrons of size $\Delta \tn{x}_s = 11 \Delta \tn{x}_f$ (right).}
    \label{ch4_fig_CWIPI_DN2}
\end{figure}
As the processors distribution on fluid side is completely changed after a re-meshing step, the coupling with CWIPI has to be entirely reestablished after each DMA.

Now that the fluid solver (Chapter~\ref{ch2}), the solid solver (Chapter~\ref{ch3}) and the mesh movement solver have been presented, next part will 
present different possible coupling scheme, using a simple test case. 

\newpage
\subsection{Weak coupling}
\label{ch4_sec_weak}
% Schéma, 
% limite, pb concret (montré courbes?)
% ex probleme de la poutre
\subsubsection{Case presentation} 
\label{ch4_sec_beam}
This part aims at explaining why a strong coupling scheme is necessary for certain cases. 
The coupling scheme used in the rest of this work will then be introduced step by step. 
For that, using a simple test case, the density ratio $\rho_s/\rho_f$ will be varied 
to highlight the instabilities induced by the added mass effect. Note that other parameters, like geometry, 
have also an influence on this effect~\cite{causin2005added,forster2007artificial},
but only $\rho_s/\rho_f$ will be discussed here. Thus, increasingly complex schemes will be tested to study their 
robustness against more and more unstable cases. It will finally lead to the actual scheme used to reproduce cases of next chapters. 
It should also be precised that more sophisticated schemes exist in the literature, and that will be discussed as perspective of this work in section~\ref{ch7_sec_perspective}.

A simple 3D case of a flexible beam in a channel has been devised to study the stability of the different schemes with low cost tests.
The case is illustrated in Fig.~\ref{ch4_fig_setup_avvt}. 
\begin{figure}[htbp]
    \centering
    \begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[width=\linewidth]{avvt_setup.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[width=\linewidth]{avvt_side_view.png}
    \end{subfigure}
    \caption{Test case configuration (top) and cross-section (bottom).}
    \label{ch4_fig_setup_avvt}
\end{figure}
The beam has a lenght $l=1$ m, a square section of side $a=0.1$ m and 
is reproduced with 10 27-nodes hexahedrons. The material properties 
are $E= 3.6$ GPa, $\nu_s=0.4$, and $\rho_s$ will be case dependent. The fluid domain dimensions are $5\tn{m} \times 4\tn{m} \times 3\tn{m}$ 
and the fluid mesh is composed by $38 \times 10^{3}$ 
tetrahedrons. The fluid properties are $\nu_f=10^{-6}\ \tn{m}^2.\tn{s}^{-1}$ and $\rho_f=10^{3}\ \tn{kg}.\tn{m}^{-3}$. 
The inflow velocity is $U_{\infty}=1\  \tn{m}.\tn{s}^{-1}$. In order to increase the structure deflection, a sinusoidal displacement of period $T=1$ s and 
amplitude $A=0.1$ m is imposed at the basis of the beam. For the mesh movement, the pseudo-solid method presented in the last section is used and 
slip boundary conditions are used for channel walls. 

The beam will then bend under the flow effect. Fluid forces applied on the structure and beam tip displacement will be used for comparison.  

\subsubsection{Parallel synchronous scheme}
The first approach proposed is very simple; data for coupling are exchanged at the beginning of the time step and both fluid and solid 
solving are computed simultaneously. The scheme is given in Fig.~\ref{ch4_fig_scheme_expl1}.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{FSI_explicit_scheme_1.png}
\caption{Parallel synchronous scheme. Fluid and solid solving are performed simultaneously.}
\label{ch4_fig_scheme_expl1}
\end{center}
\end{figure*}
The $n+1$ time iteration begins with computation of the time step value $\Delta t_f$ given the stability condition of the 
Runge–Kutta method used by fluid solver. Note that the SMS uses the generalized-$\alpha$ (see section~\ref{ch3_sec_gene}) temporal 
scheme which is unconditionally stable. 
Thus, $\Delta t_f$ is sent from the fluid to the solid and $\Delta t_s = \Delta t_f$ is imposed for the SMS. In the present case, 
it results in a time step of about 15 ms.
This method ensures optimal computational cost, but not the consistency between fluid and solid solution at the end ot the time step. 
Figure~\ref{ch4_fig_comp1} compares the results obtained during the first ten seconds with $\rho_s/\rho_f=[3.0,2.0,1.0]$.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{avvt_courbes/comp_weak.pdf}
\caption{Result obtained with the parallel synchronous scheme for different density ratios $\rho_s/\rho_f$.}
\label{ch4_fig_comp1}
\end{center}
\end{figure*}
It can clearly be seen that instabilities on forces (note that logarithmic scale is used) and $d_x$ are growing when $\rho_s/\rho_f$ decreases. 
For $\rho_s/\rho_f=1$, the forces diverges so that the computation failed. It must also be noted that important 
instabilities on the drag force reappears for $\rho_s/\rho_f=2$ even after several seconds with small variations.

This low cost scheme can be useful for simple case where a global behaviour is studied, but lacks of accuracy and 
does not allow to reproduce cas with important added mass effect. 

\subsubsection{Serial staggered scheme}
This second scheme is presented in Fig.~\ref{ch4_fig_scheme_expl2}. It is sequential and begins with an estimation of structure displacement with 
\begin{equation}
\label{eq_fsi_est}
\mathbf{d}^{0}_{n+1} = \mathbf{d}_{n} + \Delta t_{f}\mathbf{v}_{n}  
\end{equation}
where $\mathbf{v}$ is the solid velocity. 
This extrapolated displacement is then used by the ALE solver to compute fluid forces. This latter are sent to the SMS and taken into account to compute 
$\mathbf{d}_{n+1}$. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{FSI_explicit_scheme_2.png}
\caption{Serial staggered scheme.}
\label{ch4_fig_scheme_expl2}
\end{center}
\end{figure*}
This method ensures a better consistency between solutions than previous scheme, but it is not perfectly accurate as $\mathbf{d}^{0}_{n+1} \ne \mathbf{d}_{n+1}$. 
Figure~\ref{ch4_fig_comp2} compares the results obtained during the first two seconds with $\rho_s/\rho_f=[1.0,0.75,0.5]$.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{avvt_courbes/comp_niter1.pdf}
\caption{Result obtained with the serial staggered scheme for different density ratios $\rho_s/\rho_f$.}
\label{ch4_fig_comp2}
\end{center}
\end{figure*}
With this scheme, case with $\rho_s/\rho_f=1$ can be reproduced. Nonetheless, for lower density ratios, instabilities appear. They finally vanish 
for $\rho_s/\rho_f=0.75$ but they make the computation fail for $\rho_s/\rho_f=0.5$. This scheme is then slower than the previous one, but more precise; 
however, it does not allow case with important mass effect. In this test, this problem appears for very low $\rho_s/\rho_f$, but for other conditions, higher 
ratios can still result in unstable case, like in section~\ref{ch5_sec_FSI2} where $\rho_s/\rho_f=10$ but the case cannot be reproduced with a weak coupling. 

In order to enforce the equilibrium of the traction and displacement on the fluid-structure interface and ensure consistency between 
fluid and solid solutions at each time step, a strong coupling is required. 
This implies an additional iterative procedure during time advancement. The chosen strong scheme is explained in the next section.
\subsection{Partitioned semi-implicit predictor-corrector coupling scheme}
% Fernandez puis Breuer
% Voir papier
% montré courbe print_details
The FSI coupling scheme applied is given Fig.~\ref{ch4_fig_fsi_coupling_scheme} and is similar to the one proposed by Breuer et al.~\cite{breuer2012fluid}.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{FSI_implicit_scheme.png}
\caption{FSI coupling scheme used for the present work.}
\label{ch4_fig_fsi_coupling_scheme}
\end{center}
\end{figure*}
This coupling scheme is the one used in the rest of this work, so it will be explained in details. It can be split into different steps: 
\begin{enumerate}
    \item The $n+1$ time iteration begins with computation of the time step value $\Delta t_f$ given the stability condition of the 
    Runge–Kutta method used by fluid solver. Note that the SMS uses the generalized-$\alpha$ (see section~\ref{ch3_sec_gene}) temporal scheme which is unconditionally stable. 
    Thus, $\Delta t_f$ is sent from the fluid to the solid and $\Delta t_s = \Delta t_f$ is imposed for the SMS. 
    \item New displacement of the interface $\Gamma$ is estimated thanks to the following extrapolation 
    \begin{equation}
    \label{eq_fsi_3}
    \mathbf{d}^{0}_{n+1} = \mathbf{d}_{n} + \alpha_{0}\Delta t_{f}\mathbf{v}_{n} + \alpha_{1}\Delta t_f(\mathbf{v}_{n-1}-\mathbf{v}_{n}) 
    \end{equation}
    where $\mathbf{v}$ is the solid velocity, and $\alpha_i$ are interpolation coefficients. For $\alpha_{0}=1$ and $\alpha_{1}=0$, Eq.(\ref{eq_fsi_3}) 
    corresponds to a linear extrapolation while for $\alpha_{0}=1$ and $\alpha_{1}=1/2$ it corresponds to quadratic extrapolation. This last option leads to a 
    faster convergence so it is used in this work. 
    \item Mesh movement is solved thanks to the method explained in section~\ref{ch4_sec_mms} with the displacement condition $\mathbf{d}^{0}_{n+1}$. 
    That results in a nodes velocity field $\mathbf{w}_{n+1}^{0}$ used to perform the "velocity prediction" step, as detailed in section~\ref{ch2_sec_pred}. 
    The intermediate velocity $\mathbf{u}^{*}_{n+1}$ obtained is stored apart.
    \item A new pressure field is then determined with Eq.(\ref{ch2_eq_poisson}) and used to perform the "velocity correction step" via Eq.(\ref{ch2_eq_pred}). 
    With the resulting pressure and velocity fields, the fluid force applied on the solid $\mathbf{f}^{k}_{n+1}$ can be computed. Here the upper index $k$ corresponds to 
    the number of subiterations in the FSI loop. Note that absence of this index refers to converged values.
    \item The $\mathbf{f}^{k}_{n+1}$ field is sent from the fluid to the solid by using CWIPI library.
    \item The displacement $\mathbf{d}^{k}_{n+1}$ in the solid is computed using the methodology detailed in section~\ref{ch3_sec_num}.
    \item At this stage, both fluid and solid have been solved at least once. The dynamic equilibrium is then checked to verify the consistency of the two solutions. 
    This checking is performed with 
    \begin{equation}
    \label{eq_fsi_4}
    \frac{\left|\left|\mathbf{d}^{k}_{n+1}-\tilde{\mathbf{d}}^{k-1}_{n+1}\right|\right|_{\infty}}
    {\left|\left|\mathbf{d}^{k}_{n+1}-\mathbf{d}_{n}\right|\right|_{\infty}} 
    \leq \varepsilon_{FSI}
    \end{equation}  
    where $\varepsilon_{FSI}$ is a chosen convergence criterion, and $\tilde{\mathbf{d}}^{0}_{n+1}=\mathbf{d}^{0}_{n+1}$. 
    If the FSI solution is converged, the solution at the considered time is finally determined and 
    next time step can be started.
    \item Otherwise, the computed displacement has to be underrelaxed on $\Gamma$ with 
    \begin{equation}
    \label{eq_fsi_5}
    \tilde{\mathbf{d}}^{k}_{n+1} = \omega \mathbf{d}^{k}_{n+1} + (1-\omega)\mathbf{d}^{k-1}_{n+1} 
    \end{equation} 
    where $\omega$ is a constant underrelaxation factor defined by the user. 
    This value can also be computed at each subiteration by different means~\cite{kuttler2008fixed} 
    but tests for our cases show that different methods did not allow to guarantee stability. 
    Then, an accurate value of $\omega$ has to be found for the different case; 
    a too large value will make the computation diverge, or will cause instability on fluid forces. On the other hand, the lower $\omega$ is, 
    the greater will be the total number of subiterations $N_{FSI}$ required before reaching convergence. 
    \item The underrelaxed displacement $\tilde{\mathbf{d}}^{k}_{n+1}$ is then sent to the fluid via CWIPI. 
    \item A new field of $\mathbf{w}_{n+1}^{k}$ is computed thanks to the mesh movement solver (MMS). It has to be precised that 
    before the solving, the domain is put back to its position at $t_{n}$. A new increment of displacement has to be computed but the adjustment 
    of pseudo-material parameters with Eq.(\ref{ch4_eq_adj}) has to be done only once the convergence is reached. 
\end{enumerate}
With the previously stored velocity prediction $\mathbf{u}^{*}_{n+1}$ and the updated $\mathbf{w}_{n+1}^{k}$, 
new pressure and velocity fields can be determined so that step 4 can be repeated and updated fluid forces $\mathbf{f}^{k+1}_{n+1}$ can be 
computed, closing so the FSI loop. This method thus ensures stability with a partitioned coupling and can be easily implemented, 
but presents the main drawback that the computational cost will depend on the number of FSI subiterations $N_{FSI}$ necessary to reach convergence. 
This number depends on a lot of factors and is initially impossible to determined.
The skipping of the velocity prediction once in the FSI loop was initially proposed by Fernandez et al.~\cite{fernandez2007projection}. 
It allows to reduce computational cost and it has been shown that it does not affect the final result. 
Also, the underrelaxation and the convergence checking can be performed on $\mathbf{f}^{k}_{n+1}$ instead of $\mathbf{d}^{k}_{n+1}$.

The case of section~\ref{ch4_sec_beam} is now reproduced with this scheme for $\rho_s/\rho_f=0.5$. 
Numerical parameters are $\omega=0.5$ and $\varepsilon_{FSI}=10^{-3}$.
Figure~\ref{ch4_fig_curv3} shows the results. 
\begin{figure}[htbp]
    \centering
    \begin{subfigure}[b]{1.0\textwidth}
        \includegraphics[width=\linewidth]{avvt_courbes/essai14_str_500_log.pdf}
    \end{subfigure}
    \begin{subfigure}[b]{1.0\textwidth}
        \includegraphics[width=\linewidth]{avvt_courbes/essai14_detail2.pdf}
    \end{subfigure}
    \caption{Result obtained with the strong coupling scheme for $\rho_s/\rho_f=0.5$ (top) and evolution of solid and force convergence with subiterations 
    for the first 20 time iterations (bottom).}
    \label{ch4_fig_curv3}
\end{figure}
% tres stable si ce n'est petit pic au début sur le drag ==> début tjr le plus dur car le solide n'a pas d'intertie
% on retrouve ce pb at the maximum and minimum of amplitudes of d_y ==> hause du nb de subiteration
% Convergence behaviour of the force and displacement are similar
It shows that the computed solution is very accurate and whithout instabilities. Only the first time iterations presents a spike of drag, which is
due to the non physical initialization of the flow. It can also be observed on the curve of $N_{FSI}$ that solid state corresponding to the maximums and 
the minimums of $d_y$ are unstable; at these instants, structure has a small inertia so that it requires more subiterations 
to determine the two consistent solutions. This phenomenon will also be observed on cases of the next chapters. Second graph also highlights that 
displacement and force have the same convergence behaviour. 

This coupling scheme has finally been adopted for the present work. On each case, tests have to be done to find suitable values of 
$\omega$ and $\varepsilon_{FSI}$ to guarantee stability but also to minimize the computational cost. Also, in several cases, it has been noticed that 
the flow had to be established before starting the FSI coupling. 
As a matter of fact, whithout substantial resulting fluid forces on the structure, it seems that the scheme 
does not allow to find a stable solution and the successive computed forces within the FSI loop tend to diverge. This specific point has been a large source of 
difficulties in the PhD and took a lot of time to be identified. An important perspective of this work could be to develop a robust initialization strategy.

\subsection{FSI coupling with DMA}
\label{ch4_sec_FSI_DMA}
% \subsubsection{Problem}
% % interpolation ==> plus de divergence nul ==> Pression poubelle de Y2
% % faire le test et montré courbe (print_details?)
% \subsubsection{Solution}
% % tentative avec P_post
% % extrapo ça passe mais augmente N_fsi, faut pas adaptation trop rapproché
%% ex sera donné avec FSI2
It was important to be able to use DMA with the FSI solver to reproduce case with large deflection or flexible moving object. However, 
as mentioned in section~\ref{ch2_sec_DMA}, DMA can induce pressure spike in some configurations. This strong discontinuity on fluid force can disturb the 
FSI coupling and make the computation diverge. Attempts had been made to correct the pressure spike and reconstruct another pressure field after DMA but it 
was not enough to stabilize the coupling. 

The solution finally adopted consists in an extrapolation of the fluid forces at the time step after the re-meshing. It is computed as
\begin{equation}
\label{eq_fsi_DMA}
\mathbf{f}_{n+2} = 2\ \mathbf{f}_{n+1} - \mathbf{f}_{n} 
\end{equation} 
and is sent to the solid instead of the normally computed $\mathbf{f}^k_{n+2}$ at each subiteration. The underrelaxation factor is then 
temporarily set to 1 so that $\left|\left|\mathbf{d}^{2}_{n+1}-\tilde{\mathbf{d}}^{1}_{n+1}\right|\right|=0$ at the second subiteration and $N_{FSI}$ is 
systematically equal to 2 after DMA.  
This solution was very simple to implement but revealed quite efficient. Figure~\ref{ch4_fig_FSI_DMA} shows the results obtained for the previous case 
where DMA was forced every 100 time iterations. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{avvt_courbes/try2_adapt1.png}
\caption{Result obtained with the strong coupling scheme for $\rho_s/\rho_f=0.5$ with DMA.}
\label{ch4_fig_FSI_DMA}
\end{center}
\end{figure*}
The DMA step can be identified on the skewness curves by the sudden drops. It can also be noticed that at next time iteration, $N_{FSI}=2$. 
Other cases with DMA will be presented in next Chapters. 

\vspace*{2cm}

The entire numerical methodology used in the FSI solver has now been presented. The three schemes studied in this Chapter are still available in the FSI solver of YALES2. 
These developments occupied approximatively the first half of the PhD. 
It was then essential to correctly validate this new solver. Next Chapter will present the validation against a 2D laminar reference test case. 