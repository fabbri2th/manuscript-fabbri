\chapter{Validation against 2D numerical laminar cases}
\label{ch5}
\graphicspath{ {./img/ch5/}}

\textit{In this chapter, a complete validation of the previously introduced FSI solver is given, including mesh convergence studies. 
For that, a 2D numerical benchmark is reproduced. It involves the interaction between an elastic rod and a laminar incompressible flow. 
The chapter starts with validations of fluid and solid solvers independently, 
to finally presents two cases of FSI, with and without Dynamic Mesh Adaptation.}

\adjustmtc[0]

\minitoc


% voir papier de ouf
% rajouter courbe mais sinon c'est tout non?
\section{Case presentation}
In order to assess the present methodology, a 2D laminar FSI test case is first considered: 
the benchmark proposed by Turek \& Hron~\cite{turek2006proposal}. This numerical case consists in the interaction between an 
elastic rod and a laminar incompressible flow. The benchmark proposes to validate each solver independently in preliminary tests. 
Therefore, three different test cases including mesh convergence studies are presented in this part. 

This case is inspired by the older benchmark of an incompressible flow around a cylinder~\cite{schafer1996benchmark} except 
that a flexible rod is attached to the back side of the cylinder. 
The geometry and dimensions are given in Fig.~\ref{ch5_fig_Turek_setup} and Tab.~\ref{ch5_tab_Turek_table}. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{Turek_setup_legende.png}
\caption{Sketch of the studied numerical test case.}
\label{ch5_fig_Turek_setup}
\end{center}
\end{figure*}
By measuring from the left bottom corner of the channel, the cylinder center position is then $C=(0.2,0.2)$ while the rod tip is situated at $A=(0.6,0.2)$.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|}
\hline
\cellcolor[HTML]{ECF4FF}Geometry parameters         & \cellcolor[HTML]{ECF4FF}Value {[}m{]} \\ \hline
Cylinder diameter           & $ D = 0.1 $    \\
Cylinder center x-position  & $L_{c} = 0.2$     \\
Cylinder center y-position  & $H_{c} = 0.2$     \\
Channel lenght         & $L = 2.5 $    \\
Channel height         & $H = 0.41 $    \\
Deformable structure length & $l = 0.35$     \\
Deformable structure thickness & $h = 0.02$     \\ \hline
\end{tabular}
\caption{Dimensions of the sketch in Fig.~\ref{ch5_fig_Turek_setup}.}
\label{ch5_tab_Turek_table}
\end{center}
\end{table}
%
It should be noticed that the setup is intentionally non-symmetric (with $H_{c}\ne H/2$) to prevent any influence of the computation precision. 
For the three folowing tests, reference values have been taken from~\cite{turek2006proposal}. 
They are obtained with a fully implicit monolithic ALE-FEM method with a fully coupled multigrid solver~\cite{hron2006monolithic} and 
are almost grid independent. 

\section{CFD tests}
\label{ch5_sec_CFD}
To first validate the fluid solver alone, the benchmark proposes three cases with different physical parameters: 
the case named CFD3 is chosen here.  The solid is considered perfectly rigid so that the test case focuses on the laminar 
flow description around the cylinder and the attached rod. Quantities used for comparison will then be the fluid forces applied 
on the whole submerged body, computed according Eq.~(\ref{eq_fsi_2}), for a fully developed flow and for one full 
period of the oscillation. In fact, for this case, a non stationary regime is reached where pressure distribution fluctuates. \\
The flow is considered as incompressible, with a density $\rho_f=1000 \ \tn{kg.m}^{-3}$ and a kinematic viscosity 
of $\nu_{f}=0.001 \ \tn{m}^{2}.\tn{s}^{-1}$. A parabolic velocity profile $u(x=0,y)$ is prescribed at the inlet as 
\begin{equation}
\label{Turek_inlet}
u(x=0,y) = 1.5u_{\infty} \frac{y(H-y)}{(H/2)^{2}} \, .
\end{equation}
In CFD3 case, $u_{\infty}=2\  \tn{m.s}^{-1}$ leading to a Reynolds number $Re=200$ which leads to vortex shedding 
behind the cylinder, as illustrated in Fig.~\ref{Turek_FSI3}. No-slip boundary conditions are applied at channel walls and at the body. 
% diff maillage

Simulations are performed on three different meshes, $\tn{M1}_f$, $\tn{M2}_f$ and $\tn{M3}_f$ composed of 
triangles. They are characterized by two metric values, $\Delta x_{1}$ and $\Delta x_{2}$, which correspond to cell size close to the 
cylinder and the rod, and to the cell size in the rest of the domain, respectively. These values are given in Tab.~\ref{Turek_cfd_mesh}.

\begin{table}[htbp]
\begin{center}
\begin{tabular}{c|c|c|c|}
\cline{2-4}                       
                                   & \cellcolor[HTML]{ECF4FF}$\tn{M1}_f$      & \cellcolor[HTML]{ECF4FF}$\tn{M2}_f$   & \cellcolor[HTML]{ECF4FF}$\tn{M3}_f$      \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\Delta x_1$ {[}m{]}}  & $1\times 10^{-3}$  & $8\times 10^{-4}$ & $6\times 10^{-4}$    \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\Delta x_2$ {[}m{]}}  & $1.25\times 10^{-2}$ & $1\times 10^{-2}$ & $0.75\times 10^{-2}$ \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$N_{elem}$ ($\times10^{3}$)} & 73      & 98   & 173     \\ \hline
\end{tabular}
\caption{Characteristics of the three meshes used for CFD3.}
\label{Turek_cfd_mesh}
\end{center}
\end{table}

The timestep $\Delta t$ used here is computed following the Courant-Friedrichs-Levy (CFL) convective time step 
constraint by keeping CFL number $CFL = u \Delta t/\Delta x$ smaller than $0.8$. %\mymodifTF{(on met la definition = $udt/\Delta x$?)}
This leads to a time step value around $139 \ \tn{\textmu s}$, $113 \ \tn{\textmu s}$ and $84  \ \tn{\textmu s}$ 
for meshes $\tn{M1}_f$, $\tn{M2}_f$ and $\tn{M3}_f$, respectively. % pas de temps visc trois fois plus petit

As precised above, fluid forces oscillations are compared here. Amplitudes, mean value and frequencies have been computed 
for each mesh and are presented in Fig.~\ref{Turek_graph_CFD3} and in Tab.~\ref{Turek_cfd_result}. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{CFD3.pdf} \caption{Fluid forces integrated on the whole submerged body (cylinder+rod)
computed with $\tn{M3}_{f}$ and results of the reference study~\cite{turek2006proposal}. Time offset is due to different computation initializations.}
\label{Turek_graph_CFD3}
\end{center}
\end{figure*}
\begin{table*}[htbp]
\begin{center}
\begin{tabular}{c|c|c|c|c|c|}
\cline{2-6}
\multicolumn{1}{l|}{}               & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean Drag [N]}                         & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. Drag [N]}                          & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean Lift [N]}                               & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. Lift [N]}                               & \cellcolor[HTML]{ECF4FF}f  [Hz]                                                    \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}Ref~\cite{turek2006proposal}} & 439.45                                                     & 5.6183                                                      & -11.893                                                      & 437.81                                                      & 4.3956                                                     \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M1}_f$}           & \begin{tabular}[c]{@{}c@{}}433.69 \\ (1.31\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}6.4297 \\ (14.44\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}-7.0625 \\ (40.62\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}485.57 \\ (10.91\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}4.4326 \\ (0.84\%)\end{tabular} \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M2}_f$}           & \begin{tabular}[c]{@{}c@{}}435.35 \\ (0.93\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}6.3794 \\ (13.55\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}-7.4314 \\ (37.51\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}478.94 \\ (9.39\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}4.4347\\  (0.89\%)\end{tabular} \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M3}_f$}           & \begin{tabular}[c]{@{}c@{}}435.01 \\ (1.00\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}6.0761 \\ (8.15\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}-10.505 \\ (11.67\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}462.78 \\ (5.70\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}4.4373 \\ (0.95\%)\end{tabular} \\ \hline
\end{tabular}
\caption{Results of the CFD3 benchmark and comparison with the reference study~\cite{turek2006proposal}.}
\label{Turek_cfd_result}
\end{center}
\end{table*}
Despite that the time-steps are about 50 times smaller than in the reference case, amplitudes of drag and lift and mean 
lift converge towards the reference data when the grid is refined. Note that the mean drag force and the frequency are 
less sensitive to mesh refinement with an error about of $1 \%$ even for the coarser mesh. Therefore, the results allow to consider 
that the CFD solver is validated. 

\section{CSM tests}
\label{ch5_sec_csm_tests}
% cas rapide
The solid solver needs now to be validated with pure structural test. This test consists in computing the deformation of the flexible rod in a gravitational field $g = (0,2) \ \tn{m}.\tn{s}^{-2}$ without taking into account the fluid. 
In this study, the CSM3 test is chosen because it is the only time dependent case. It starts from the undeformed configuration and as there is no damping, the structure immediately oscillates periodically. The material is characterized by a Poisson's ratio of $\nu_s=0.4$, a Young modulus of $E=1.4 \ \tn{MPa}$ and a density of $\rho_s = 1000 \ \tn{kg}.\tn{m}^{-3}$. As significant deformations are expected, the  Saint-Venant-Kirchhoff material model is used.

% diff maillage
Three meshes have been made for this test, corresponding only to the deformable part of the body because the cylinder is always considered perfectly rigid. These meshes are only composed by 9-nodes quadrilaterals of size $\Delta x$ given in Tab.~\ref{Turek_csm_mesh}.

\begin{table}[htbp]
\begin{center}
\begin{tabular}{c|c|c|c|}
\cline{2-4}
                                   & \cellcolor[HTML]{ECF4FF}$\tn{M1}_s$      & \cellcolor[HTML]{ECF4FF}$\tn{M2}_s$   & \cellcolor[HTML]{ECF4FF}$\tn{M3}_s$      \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\Delta x$ {[}m{]}}  & $4\times10^{-2}$  & $2\times10^{-2}$ & $1\times10^{-2}$    \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$N_{elem}$ } & 9      & 18   & 70     \\ \hline
\end{tabular}
\caption{Characteristics of the three meshes used for CSM3.}
\label{Turek_csm_mesh}
\end{center}
\end{table}

% param numérique (spectral radius)
As the structure is clamped to the backside of the cylinder, a non displacement boundary condition is applied to the corresponding nodes. 
The chosen time step is $\Delta t=0.005 \ \tn{s}$ (same than in ~\cite{turek2006proposal}) and is applied with 
the generalized-$\alpha$ method with the spectral radius $\rho_{\infty}=0.8$ (Eq.(\ref{ch3_eq_spec_radius})).

% results
For comparison, displacement of previously defined point A is tracked in time. Once again, mean values, 
amplitudes and frequencies are computed for displacements $d_x$ and $d_y$ along $x$-axis and $y$-axis, respectively. 
All results are gathered in Tab.~\ref{Turek_csm_result} and in Fig.~\ref{Turek_graph_CSM3}. 
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{CSM3.pdf} \caption{Displacement of point A along $x$-axis (top) and $y$-axis (bottom) 
obtained with $\tn{M3}_{s}$ and results of the reference study~\cite{turek2006proposal}.}
\label{Turek_graph_CSM3}
\end{center}
\end{figure*}
\begin{table*}[htbp]
\begin{center}
\begin{tabular}{c|c|c|c|c|c|}
\cline{2-6}
\multicolumn{1}{l|}{}     & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean $d_x$ \ [mm]}                        & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. $d_x$ \ [mm]}                       & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean $d_y$ \ [mm]}                        & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. $d_y$ \ [mm]}                       & \cellcolor[HTML]{ECF4FF}f  \ [Hz]                                                \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}Ref~\cite{turek2006proposal}} & -14.305                                                     & 14.305                                                     & -63.607                                                     & 65.160                                                     & 1.0995                                                     \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M1}_s$} & \begin{tabular}[c]{@{}c@{}}-14.057 \\ (1.73\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}14.057\\ (1.73\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}-63.367 \\ (0.38\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}63.367 \\ (2.66\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}1.1074\\ (0.72\%)\end{tabular}  \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M2}_s$} & \begin{tabular}[c]{@{}c@{}}-14.398\\ (0.65\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}14.394 \\ (0.63\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}-64.113\\ (0.80\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}64.436\\ (1.10\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}1.1053\\  (0.53\%)\end{tabular} \\ \hline
\multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M3}_s$} & \begin{tabular}[c]{@{}c@{}}-14.257\\ (1.06\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}14.452\\ (1.03\%)\end{tabular}  & \begin{tabular}[c]{@{}c@{}}-64.163 \\ (0.87\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}64.872 \\ (0.44\%)\end{tabular} & \begin{tabular}[c]{@{}c@{}}1.0972 \\ (0.21\%)\end{tabular} \\ \hline
\end{tabular}
\caption{Results of the CSM3 benchmark and comparison with the reference study~\cite{turek2006proposal}.}
\label{Turek_csm_result}
\end{center}
\end{table*}

% Comparison with reference values is here very convincing, even though grid used for reference study is much finer. 
Differences with reference values are here minor, even though grid used in the reference study is much finer (5120 elements). 
Efficiency of 9-nodes quadrilaterals seems here confirmed since errors are not above 2\% for all quantities, except 
for the amplitude of $d_y$ obtained with $\tn{M1}_s$, which converges with grid refinement anyway. 
This close agreement with reference data allows then to successfully validate the CSM solver. 

\section{FSI tests}
\subsection{FSI3 case}
Finally, to validate the coupling between fluid and solid solvers, the FSI3 benchmark has been reproduced. 
The fluid forces are now applied to the flexible structure so that it deforms and starts interacting with the flow. 
Fluid properties are the same as in CFD3 but for the solid, the Young modulus is chosen as $E = 5.6 \ \tn{MPa}$.
% pas facile car  meme densité et channel très étroit pas évident pour MMS
This case is particularly challenging because $\rho_f = \rho_s = 1000 \ \tn{k.m}^{-3}$, 
which maximizes the importance of the added-mass effect. Besides, the channel is narrow compared to the expected structure deflections; 
that imposes a very efficient mesh movement algorithm to keep a low maximum skewness.   
% param numérique (param mms? conv fsi, omega)
% result, pas besoin dadapt, nb de sous ité
The method presented in section~\ref{ch4_sec_mms} is used here with $R_{min}=0.02\ \tn{m}$, $R_{max}=0.2 \ \tn{m}$ and $y_r=100 \ \tn{m}$. 
These values allow the most refined zone close to the body to remain intact while coarser regions close to the channel walls 
will withstand the deformation. That can be seen in Fig.~\ref{Turek_FSI3} which shows the resulting grid $\tn{M1}_f$ 
when the rod is at the maximum deflection. The method ensures a good grid quality without needs of DMA step.
As regards the FSI coupling, the convergence criterion defined in Eq.~(\ref{eq_fsi_4}) is set at $\varepsilon_{FSI}=1\times 10^{-5}$ 
and the underrelaxation factor is $\omega=0.1$. Because of the density ratio $\rho_s/\rho_f=1$, higher values of $\omega$ do not 
allow to reach convergence within the FSI loop. Nonetheless, it results in a relatively low mean number of subiterations $N_{FSI}$ 
of 15.04 while this number averaged 55.21 in~\cite{breuer2012fluid} for the same value of $\omega$. 
However, Breuer et al.~\cite{breuer2012fluid} managed to reach convergence with $\omega=0.5$ and $N_{FSI}=9.38$ which was not 
possible in the present study.% \mymodifTF{pas sûr que la comparaison avec DN soit nécessaire nn?}%enlever la comp avec DN?

In the original case, the authors propose to progressively establish the flow starting with a zero velocity field in all the domain. 
Note that this cannot be done with the present algorithm because the fluid forces were so low that the importance of the added mass effect 
was relatively too important. Such an unstable configuration surely requires a more stable monolithic approach. 
Therefore, the coupling has here been started with a fully developed flow, as computed in CFD3 case.
% It must also be precised that the coupling has here been started with a fully developed flow, as computed in CFD3 case. The original case yet proposes to progressively establish the flow, starting with a zero velocity field in all the domain. This could not be computed with the present algorithm because the fluid forces were so low that the importance of the added mass effect was relatively too important. Such an unstable configuration surely requires a more stable monolithic approach. % ou pas?

%force ou déplacement (plutot déplacement comme dans cas DN) comp autre equipe? figure
Figure~\ref{Turek_FSI3} also depicts an instantaneous velocity field. The vortex shedding at the cylinder is clearly visible and induces 
oscillating forces applied in the entire body as it has been seen in the CFD3 case of section~\ref{ch5_sec_CFD}. 
These forces cause the flexible structure displacement resulting in periodic displacement of the rod behind the cylinder. 

\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{paper_FSI3.png} \caption{Velocity field and deformed mesh of 
FSI3 benchmark reproduced with $M1_{f}$ $\&$ $\tn{M1}_{s}$. Note that the domain has been cropped on the right side to focus on the deformable part.}
\label{Turek_FSI3}
\end{center}
\end{figure*}


\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{comp_y2_turek_L.png} \caption{Displacement of point A along $x$-axis (top) and $y$-axis (bottom) 
obtained with $\tn{M3}_{f}\&\tn{M3}_{s}$ and results of the reference study~\cite{turek2006proposal}.}
\label{Turek_graph}
\end{center}
\end{figure*}

\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{result_FSI3.png} 
\caption{Results of the FSI3 benchmark and comparison with the reference study~\cite{turek2006proposal}.}
\label{Turek_fsi_result}
\end{center}
\end{figure*}

% \begin{table*}%[htbp]
% \begin{center}
% \begin{tabular}{c|c|c|c|c|c|c|}
% \cline{2-7}
% \multicolumn{1}{l|}{}                               & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean $d_x$ \ {[}mm{]}} & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. $d_x$ \ {[}mm{]}} & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}f  $d_x$ \ {[}Hz{]}} & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Mean $d_y$ \ {[}mm{]}} & \multicolumn{1}{l|}{\cellcolor[HTML]{ECF4FF}Amp. $d_y$ \ {[}mm{]}} & \cellcolor[HTML]{ECF4FF}f  $d_y$ \ {[}Hz{]}                   \\ \hline
% \multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}Ref~\cite{turek2006proposal}}   & -2.69                                                        & 2.53                                                         & 10.9                                                        & 1.48                                                         & 34.38                                                        & 5.3                                                      \\ \hline
% \multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M1}_{f}\&\tn{M1}_{s}$} & \begin{tabular}[c]{@{}c@{}}-3.08 \\ (14.37\%)\end{tabular}   & \begin{tabular}[c]{@{}c@{}}2.86\\ (12.91\%)\end{tabular}     & \begin{tabular}[c]{@{}c@{}}11.1 \\ (1.43\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}1.41 \\ (4.83\%)\end{tabular}     & \begin{tabular}[c]{@{}c@{}}36.22 \\ (5.35\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}5.53 \\ (4.28\%)\end{tabular} \\ \hline
% \multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M2}_{f}\&\tn{M2}_{s}$} & \begin{tabular}[c]{@{}c@{}}-3.06\\ (13.57\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}2.82 \\ (11.61\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}11.02\\  (1.08\%)\end{tabular}   & \begin{tabular}[c]{@{}c@{}}1.30\\ (12.17\%)\end{tabular}     & \begin{tabular}[c]{@{}c@{}}36.16\\ (5.16\%)\end{tabular}     & \begin{tabular}[c]{@{}c@{}}5.51\\  (3.92\%)\end{tabular} \\ \hline
% \multicolumn{1}{|c|}{\cellcolor[HTML]{ECF4FF}$\tn{M3}_{f}\&\tn{M3}_{s}$} & \begin{tabular}[c]{@{}c@{}}-3.01\\ (11.76\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}2.77\\ (9.60\%)\end{tabular}      & \begin{tabular}[c]{@{}c@{}}10.99\\ (0.82\%)\end{tabular}    & \begin{tabular}[c]{@{}c@{}}1.42\\ (4.24\%)\end{tabular}      & \begin{tabular}[c]{@{}c@{}}35.89\\ (4.40\%)\end{tabular}     & \begin{tabular}[c]{@{}c@{}}5.49 \\ (3.63\%)\end{tabular} \\ \hline
% \end{tabular}
% \caption{Results of the FSI3 benchmark and comparison with the reference study~\cite{turek2006proposal}.}
% \label{Turek_fsi_result}
% \end{center}
% \end{table*}

For comparison, sinusoidal displacement of previously defined point A is measured and presented in Fig.~\ref{Turek_graph} and in Tab.~\ref{Turek_fsi_result}.
Even if numerical methodologies between the present study and the reference one are different, 
the results converge toward the reference values. The only disparity occurs for the mean $d_y$ obtained 
with mesh $\tn{M2}_{f} \& \tn{M2}_{s}$. Considering that it concerns a small value and that error 
obtained with mesh $\tn{M3}_{f} \& \tn{M3}_{s}$ drops below the one with $\tn{M1}_{f} \& \tn{M1}_{s}$, 
it can be considered as a minor deviation. Consequently the overall agreement with reference data enables to validate the present FSI solver.  


More data are given for the computation with $\tn{M3}_{f} \& \tn{M3}_{s}$ in Fig.~\ref{ch5_fig_FSI3}. This has been performed with 
16 CPU for the ALE solver and 1 for the SMS; the mesh movement solving took about 61\% of the computational time, the fluid solving 26\% and solid solving 13\%.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{FSI3_M2f_M3s_new.pdf} \caption{Results obtained for FSI3 with $M3_{f}$ $\&$ $\tn{M3}_{s}$.}
\label{ch5_fig_FSI3}
\end{center}
\end{figure*}
With the three first graphes, it can be deduced that the computed solution is very stable. As regards the mesh movement, the nearly constant mean skewness in 
the domain confirms that only few cells are problematic. As for the maximum skewness, it slowly increases because of the non reversibility of 
the algorithm already highlighted in Fig.~\ref{ch4_fig_courbes}. However, the maximum skewness finally reaches values where the variation are stable and 
which allow the computation to continue despite high skewness. The cycles observed of the number of subiterations $N_{FSI}$ were already observed in 
Fig.~\ref{ch4_fig_curv3}. It corresponds to the same oscillations than $d_y$. 

% \subsection{Comparison with other teams}
\subsection{FSI2 case}
\label{ch5_sec_FSI2}

Another proposed benchmark FSI2~\cite{turek2006proposal} has also been reproduced and statisfying results have been obtained. 
In this test case, $\rho_s / \rho_f = 10$ which makes the coupling less unstable, but it still requires the strong coupling scheme. 
This allows to apply an underrelaxation of $\omega=0.3$ and convergence is reached with a number of FSI subiterations that averages 4.5. 
Contrary to the FSI3 case, here the larger deflections impose the use of the DMA, which took less than 0.1\% of the computational time.
Animation of this case can be visualized at \url{https://www.youtube.com/watch?v=qLWOg\_EJtTM}. 
With $M2_{f}$ $\&$ $\tn{M3}_{s}$, the amplitudes were predicted with less than 2.3\% of differences 
and frequencies with less than 4.5\% in comparison with~\cite{turek2006proposal}.


\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{Manus_FSI2.png} \caption{Velocity field and deformed mesh of 
FSI2 benchmark reproduced with $M2_{f}$ $\&$ $\tn{M3}_{s}$. Note that the domain has been cropped on the right side to focus on the deformable part.}
\label{ch5_fig_FSI2_para}
\end{center}
\end{figure*}
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{FSI2_M1f_M3s.pdf} \caption{Results obtained for FSI2 with $M2_{f}$ $\&$ $\tn{M3}_{s}$.}
\label{ch5_fig_FSI2}
\end{center}
\end{figure*}
Results are given in Fig.~\ref{ch5_fig_FSI2_para} and in Fig.~\ref{ch5_fig_FSI2}.
Here the effect of DMA on pressure mentionned in section~\ref{ch4_sec_FSI_DMA} can clearly be seen on the two first graphes. At the time iteration after DMA, 
the Eq.(~\ref{eq_fsi_DMA}) is then used, allowing the pursuit of the computation. Nevertheless, this destabilizes the solution as it can be deduced from the 
graph of $N_{FSI}$. The effect eventually vanishes but still increases the computational cost. 

\vspace*{2cm}

The entire FSI solver has thus been validated with success on a reference benchmark. It can be concluded that the strong coupling scheme 
developed in this work allows to reproduce accurately cases with important added mass effect. Also, the FSI2 case confirms that the DMA can be used for FSI coupling, 
allowing to reproduce case with large deflection on unstructured grid. Nonetheless, the objective of this work is to reproduce 3D experimental turbulent cases, 
as chordwise flexible blade for instance. The FSI solver then needs to be validated with cases of that kind; this will be presented in the next Chapter. 