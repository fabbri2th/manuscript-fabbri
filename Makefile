# Makefile

# =====================================
FILENAME := these
# =====================================

TEXROOTFILE := $(FILENAME).tex
BIBAUXFILE := $(FILENAME).aux

TMPFILES := *.aux *.bbl *.blg *.log *.out *.toc *~ *.maf *.mtc* *.ptc

TC := `which pdflatex`
BC := `which bibtex`

# =====================================

SHELL := /bin/bash

all:
	$(TC) $(TEXROOTFILE)
	$(BC) $(BIBAUXFILE)
	$(TC) $(TEXROOTFILE)
	$(TC) $(TEXROOTFILE)

clean:
	rm -f $(TMPFILES)
	(cd acknowledgments && rm -f $(TMPFILES)) 
	(cd ch1 && rm -f $(TMPFILES)) 
	(cd ch2 && rm -f $(TMPFILES)) 
	(cd ch3 && rm -f $(TMPFILES)) 
	(cd ch4 && rm -f $(TMPFILES)) 
	(cd ch5 && rm -f $(TMPFILES)) 
	(cd ch6 && rm -f $(TMPFILES)) 
	(cd ch7 && rm -f $(TMPFILES)) 
	(cd app1 && rm -f $(TMPFILES)) 
	(cd app2 && rm -f $(TMPFILES)) 
	(cd app3 && rm -f $(TMPFILES)) 
	(cd app4 && rm -f $(TMPFILES)) 
	(cd app5 && rm -f $(TMPFILES)) 
	(cd app6 && rm -f $(TMPFILES)) 
	(cd img/ch1 && rm -f $(TMPFILES)) 
	(cd img/ch2 && rm -f $(TMPFILES)) 
	(cd img/ch3 && rm -f $(TMPFILES)) 
	(cd img/ch4 && rm -f $(TMPFILES)) 
	(cd img/ch5 && rm -f $(TMPFILES)) 
	(cd img/ch6 && rm -f $(TMPFILES)) 
	(cd img/ch7 && rm -f $(TMPFILES)) 
	(cd img/app1 && rm -f $(TMPFILES)) 
	(cd img/app2 && rm -f $(TMPFILES)) 
	(cd img/app3 && rm -f $(TMPFILES)) 
	(cd img/app4 && rm -f $(TMPFILES)) 
	(cd img/app5 && rm -f $(TMPFILES)) 
	(cd img/app6 && rm -f $(TMPFILES))


