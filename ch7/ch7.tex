\chapter{Conclusion \& Perspectives}
\label{ch7}
\graphicspath{ {./img/ch7/}}

\section{Conclusion}

Considering the necessity of energy transition, the importance to develop and enhance low carbon power sources as renewable energies keeps rising.
In this context, recent experimental studies tend to show that the use of chordwise flexible blades for
vertical axis turbines enhances their efficiencies and increases their lifetime. These potential improvements of turbine efficiency 
seem to be explained by the impact of the chordwise flexibility on dynamic stall which implies a drag reduction.

However, the FSI phenomenon occuring in these cases remains unclear in some 
aspects. Numerical simulations of these experiments would allow a better understanding of this effect and would open new perspectives 
for future improvements in the blade design. 
As a result, the main motivation of this thesis is the numerical study of VAT with chordwise flexible blades. 

Although FSI phenomena are very common in nature and human activities, their numerical simulations remain a challenging task. It requires advanced solvers 
in different physic fields. Moreover, the resulting coupling can be very unstable, especially when fluid and solid densities are close. Different approaches are possible 
to face this problem, but cases of chordwise flexible blade involve a FSI solver with specific characteristics. 
For the fluid, it is necessary to use body fitted meshes to describe 
accurately the boundary layer at high Reynolds number. As regards the solid, the foil geometry imposes the use of 3D solid element in order to compute 
structure displacements. 
Finally, both solvers have to be strongly coupled to ensure consistency between fluid and solid solutions. 
These requirements explain the limited number of numerical studies about flexible foil. 

Some FSI solvers have yet been developed to face this challenge, but a RANS approach is systematically used. This do not always provide reliable 
results because the pressure distribution is not accurate enough to conveniently determine dynamic stall
separation point position. This is why Hoerner et al.~\cite{hoerner2019characteristics} suggest that three-dimensional LES approach is 
required to reproduce with high fidelity a high Reynolds number case involving a chordwise flexible blade. 
In fact, very few LES-based FSI solvers have been developed, and they never used 3D solid elements, making a chordwise flexible foil simulation impossible. 
To the best of author’s knowledge, LES-based FSI solvers meeting all the previous requirements do not exist in the literature so far.

The aim of this work is then to develop a high fidelity FSI solver, able to reproduce a wide variety of FSI configurations, especially cases 
involving chordwise flexible blades. This solver uses LES approach to predict fluid dynamics and 
3D solid finite elements for solid dynamics, all on unstructured meshes (Chapter~\ref{ch1}). 

All the implementations have been carried out within YALES2, a multi-physics library, initially designed for fluid mechanics. 
In order to predict the flow behaviour on moving grids, specific numerical methods are used by the ALE solver. 
They have been presented, as well as a mesh movement strategy using Dynamic Mesh Adaptation (DMA), allowing to reproduce cases of moving or rotating objects. 
A simulation of a four rigid blades VAT has then been performed, confirming that the ALE solver is effective and can be used for FSI coupling (Chapter~\ref{ch2}). 

Unlike the fluid solver, the solid solver has been developed from scratch in this work. This has represented one of the main task of this thesis. 
The FEM has been explained in details, and the SMS has been validated carefully with several tests (Chapter~\ref{ch3}). 

Also, body fitted techniques involve a fluid mesh movement computation. This can be difficult for large deformations, especially with 
unstructured grids.
An original and efficient pseudo-solid method has then been proposed, meeting all the underlying requirements. 
The FSI coupling scheme has also been introduced step by step using a simple test case. The benefit of strong coupling has then been highlighted. 
The potential use of all the previous elements combination, including DMA, has thus been shown (Chapter~\ref{ch4}). 

Nonetheless, the developed FSI solver was requiring validation. 
For this purpose, a reference 2D numerical benchmark with laminar flow has been reproduced. 
For each cases, mesh convergence studies have been performed.
After validation of fluid and solid solvers independently, the 
coupling has been validated against two FSI cases, with and without DMA (Chapter~\ref{ch5}). 

The methodology has then been applied to 3D experimental turbulent cases. 
The Kalmbach case has been studied. It consists of a flexible rubber structure with an attached steel weight clamped behind a cylinder. 
Once the structure is immersed in high Reynolds number flow, large deformations of the structure are obtained.
This case has been succesfully reproduced even though it had never been simulated with unstructured fluid mesh or 3D solid finite elements by the past. 
That confirms the ability of the developed FSI solver to reproduce with high fidelity 3D turbulent cases.

Simulation of experiment involving chordwise flexible blade has been finally made at the end of the thesis. 
Even if simplification has been made for the solid problem, this confirms that the FSI solver was able 
to reproduce cases of flexible foil, completing the initial goal the thesis. 
The use of the LES approach also allowed physical analysis, providing deeper understanding of the link between the dynamic stall and the foil deformation. 
Simulation of this kind had never been done with a 3D LES approach.
This work has demonstrated thus the potential of the FSI solver for its intended use (Chapter~\ref{ch6}). 

The development of a high fidelity FSI solver seems a sucess, and despite possible improvements detailed in the next sections, 
it can be used as a tool to reproduce a wide variety of FSI configurations. 

The PhD defense can be watched at \url{https://www.youtube.com/watch?v=Kc7vQDwIQP8&t=2422s}.

\newpage

\section{Perspectives}
\label{ch7_sec_perspective}
% Meilleur preconditionner (PETSc? cf fin chap3)
% Meilleur Newton
% autre type d'EF (coque)
% plusieurs type d'EF en même temps
% autre MMS (RBF), McPhee? , Intergrille (ref UTC)
% meilleur strategie d'init FSI
% Meilleur methodes de couplage? quasi Newton? voir truc de Cyril preCICE, review partionned IQN-ILS
% plusieurs objet en chute libre thanks to DMA
% appli : turbine entière 4 pales déformables  (McPhee)

As mentionned above, the principle axis of improvement is about computational time reduction.
Most possible improvements are actually related to the SMS. 
In fact, this solver has been developed from scratch to ensure compatibility between solvers and to easily modify the FSI solver for future works. 
However, all features and advanced methods used in the literature for computational structural dynamics could not be implemented in this work. 
That is why the solid solver can be upgraded with well-known methods. 

For faster computation, the linear solving algorithm needs to be improved with a better preconditionner. As detailed in section~\ref{ch3_sec_improv}, 
One relevant choice providing many opportunities would be to make available the use of the PETSc library by the SMS~\cite{petsc-web-page,petsc-user-ref,petsc-efficient}. 
% a relevant choice that would offer a lot of possibilities would be to make available the use of the PETSc library by the SMS~\cite{petsc-web-page,petsc-user-ref,petsc-efficient}. 
Also, it would be useful to implement more advanced techniques than the basic Newton algorithm used presently for non linear solving~\cite{zienkiewicz2005nonlin}. 
Finally, development should be undertaken to allow the use of different types of finite elements in the computational domain, or also 
other types, as shell or membrane elements which are well suited to describe thin structures. 

As regards the mesh movement method, the pseudo-solid algorithm could be improved. 
Its computational cost can indeed be very important, as in section~\ref{ch6_sec_kalmbach}. 
Inspired from existing techniques~\cite{lefranccois2008simple}, 
implementations to compute mesh deformation on a coarse grid and then interpolate on the computational grid have been undertaken, but not completed. 
In addition, other mesh movement methods for unstructured grid, as techniques based on Radial Basis Functions (RBF), 
could be tested and may result in lower computational cost~\cite{sheng2013efficient}.

Another aspect slowing down computations is the large number of FSI subiterations $N_{FSI}$ required into the FSI loop. 
Only Aitken method has been tested in this work, but a lot of other 
schemes for partitioned simulation exist~\cite{degroote2013partitioned,bogaers2014quasi,blom2017efficient,naseri2018semi}. A possible solution 
would be to combine YALES2 with the preCICE library~\cite{bungartz2016precice}.

Further works could aim at implementing the features to simulate simultaneously several FSI with different objects in the same domain. 
For instance, DMA could be used to reproduce several flexible falling objects, which is very difficult with body fitted techniques. 
Also, this would be necessary to simulate an entire flexible blades VAT. 

\vspace*{2cm}

This work will also be used as a base for future studies, considering that the selected methods can be applied to a wide variety of FSI cases. 
Further works will be undertaken in order to complete the flexible foil simulations, and provide new physical insights on the use of 
chordwise flexible blade for VAT applications. 

On the other hand, at IMAG (Institut Montpelliérain Alexander Grothendieck), a thesis has been started by B. Thibaud about numerical simulation of hemodynamics in deep 
vein valves. In this work, fluid dynamics and structure dynamics are at stake respectively for blood and valve tissues. The 
FSI solver is then used to predict the deformations of the leaflets under real blood pressure condition, as illustrated in Fig.~\ref{ch7_fig_bart}.
\begin{figure*}[htbp]
\begin{center}
\includegraphics[width=1.0\linewidth]{Bart.png}
\caption{Simulations performed at IMAG with the FSI solver. 
Top: Leaflets in closed and open positions, the arrow represents the blood effect. 
Bottom: Flow velocity field in the middle place of the vein. Courtesy B. Thibaud.}
\label{ch7_fig_bart}
\end{center}
\end{figure*}

The work performed in this thesis has thus provided an advanced tool to the YALES2 community to lead new research studies in the domain of structural mechanics 
and fluid-structure interaction. 

% solver utilisé par d'autre labo pour application diff, genre bart





% rajouter courbes Turek CSM3 CFD3 FSI3 FSI2

% rajouter image CFD3 CSM3 et FSI2

% rajouter courbes DN2

% parler de DN1

% tableau % chaque solveur pur Turek et DN

% soucis de forme : 
% - page de biblio a la fin 
% - contents en haut des pages du preambule